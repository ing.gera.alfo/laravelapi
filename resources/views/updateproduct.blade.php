<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <script src="https://cdn.tailwindcss.com"></script>
        <!-- Styles -->
        <style>
            
        </style>
    </head>
    <body class="antialiased">
        <div class="container m-auto mt-5">
            <div class="relative overflow-x-auto shadow-md sm:rounded-lg p-5">
                <caption class="p-5 text-lg font-semibold text-left text-gray-900 bg-white dark:text-white dark:bg-gray-800">
                    <div class="flex items-center justify-between p-4 font-semibold text-lg">
                        Editar producto: {{$producto->nombre}}
                    </div>
                </caption>
            </div>
            <div class="relative overflow-x-auto shadow-md sm:rounded-lg p-5 mt-5">
               <form action="{{ url('product_update/'.$producto->id) }}" method="POST" >
                    @include('_form')
                </form>
            </div>
        </div>
        


    
        
        
    </body>
</html>
