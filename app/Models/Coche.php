<?php

namespace App\Models;

class Coche
{
    public $marca;
    public $modelo;

    public function mostrarInformacion()
    {
        return "Marca: " . $this->marca . "<br> Modelo: " . $this->modelo . "<br>";
    }
}