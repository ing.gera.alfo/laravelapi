<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    use HasFactory;

    public function getUpdateAtAttribute(){
        return $this->updated_at->format('d/m/Y H:m:s');
    }
}
