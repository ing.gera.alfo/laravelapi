<?php

namespace App\Http\Controllers;

use App\Models\Productos;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Models\Coche;

class ProductosController extends Controller
{
    public function index() 
    {
        $coche = new Coche();
        $coche->marca = "Toyota";
        $coche->modelo = "Corolla";
        $data = $coche->mostrarInformacion();
        
        return view('index', [
            'productos' => Productos::latest()->paginate(),
            'contenido' => Storage::get('public/datos.txt'),
            'coche'=>$data
        ]);
    }

    public function update($id) 
    {
        return view('updateproduct', [
            'producto' => Productos::find($id)
        ]);
    }

    public function create(Productos $productos) 
    {
        return view('createproduct',['producto'=>$productos]);
    }

    public function store(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|unique:productos|max:255',
            'precio' => 'required',
            'cantidad' => 'required',
        ]);
    
        if ($validator->fails()) {
            return response()->json(['error' => 'Falta información o nombre duplicado'], 400);
        }
    
        
        $producto = new Productos;
        $producto->nombre =  $request['nombre'];
        $producto->precio =  $request['precio'];
        $producto->cantidad = $request['cantidad'];
        $producto->slug = $request['nombre'];
        $producto->save();
    
        return redirect('/');
    }

    public function edit($id,Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|unique:productos|max:255',
            'precio' => 'required',
            'cantidad' => 'required',
        ]);
    
        if ($validator->fails()) {
            return response()->json(['error' => 'Falta información o nombre duplicado'], 400);
        }

        $producto = Productos::find($id);
        if (!$producto) {
            return response()->json(['error' => 'El producto no existe'], 404);
        }

        $producto->nombre =  $request['nombre'];
        $producto->precio =  $request['precio'];
        $producto->cantidad = $request['cantidad'];
        $producto->slug = $request['nombre'];
        $producto->update();
        return redirect('/');
        
    }

    
    public function destroy($id)
    {
        $producto = Productos::findOrFail($id);
        $producto->delete();

        return redirect('/');
    }

}
