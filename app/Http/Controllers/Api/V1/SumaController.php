<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\V1\UserResource;

class SumaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'num1' => 'required|numeric',
            'num2' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => 'algo salio mal'], 400);
        }

       $suma = $request['num1']+ $request['num2'];
        return response()->json($suma);
    }

    public function index()
    {
        $nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        $pares = [];
        foreach ($nums as $num) {
            if ($num % 2 == 0) {
                $pares[] = $num;
            }
        }
        return response()->json($pares);
    }

    
}
