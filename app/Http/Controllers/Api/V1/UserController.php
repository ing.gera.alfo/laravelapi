<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\V1\UserResource;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return UserResource::collection(User::latest()->paginate());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => 'Falta información o nombre duplicado'], 400);
        }

        
        $user = new User;
        $user->name =  $request['name'];
        $user->email =  $request['email'];
        $user->password = $request['password'];
        $user->save();

        return response()->json(['message' => 'User creado exitosamente']);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        
            $user = User::find($id);
            if (!$user) {
                return response()->json(['error' => 'El user no existe'], 404);
            }

            return new UserResource($user);
        
    }

    /**
     * Update the specified resource in storage.
     */
    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required',
            'password' => 'required',
        ]);
    
        if ($validator->fails()) {
            return response()->json(['error' => 'Falta información o nombre duplicado'], 400);
        }

        $user = User::find($id);
        if (!$user) {
            return response()->json(['error' => 'El producto no existe'], 404);
        }

        $user->name =  $request['name'];
        $user->email =  $request['email'];
        $user->password = $request['password'];
        $user->update();

        return response()->json(['Mensaje'=>'se actualizo correctamente']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json(['error' => 'El usero no existe'], 404);
        }

        $user->delete();

        return response()->json(['Mensaje'=>'usero eliminado correctamente']);
    }
}
