<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Productos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\V1\ProductosResource;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return ProductosResource::collection(Productos::latest()->paginate());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
{
    $validator = Validator::make($request->all(), [
        'nombre' => 'required|unique:productos|max:255',
        'precio' => 'required',
        'cantidad' => 'required',
    ]);

    if ($validator->fails()) {
        return response()->json(['error' => 'Falta información o nombre duplicado'], 400);
    }

    
    $producto = new Productos;
    $producto->nombre =  $request['nombre'];
    $producto->precio =  $request['precio'];
    $producto->cantidad = $request['cantidad'];
    $producto->slug = $request['nombre'];
    $producto->save();

    return response()->json(['message' => 'Producto creado exitosamente']);
}

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        
            $producto = Productos::find($id);
            if (!$producto) {
                return response()->json(['error' => 'El producto no existe'], 404);
            }

            return new ProductosResource($producto);
        
    }

    /**
     * Update the specified resource in storage.
     */
    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|unique:productos|max:255',
            'precio' => 'required',
            'cantidad' => 'required',
        ]);
    
        if ($validator->fails()) {
            return response()->json(['error' => 'Falta información o nombre duplicado'], 400);
        }

        $producto = Productos::find($id);
        if (!$producto) {
            return response()->json(['error' => 'El producto no existe'], 404);
        }

        $producto->nombre =  $request['nombre'];
        $producto->precio =  $request['precio'];
        $producto->cantidad = $request['cantidad'];
        $producto->slug = $request['nombre'];
        $producto->update();

        return response()->json(['Mensaje'=>'se actualizo correctamente']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $product = Productos::find($id);

        if (!$product) {
            return response()->json(['error' => 'El producto no existe'], 404);
        }

        $product->delete();

        return response()->json(['Mensaje'=>'producto eliminado correctamente']);
    }
}
