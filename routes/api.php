<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('v1/productos', App\Http\Controllers\Api\v1\ProductosController::class)
->only(['index','show','destroy','store','update']);

Route::apiResource('v1/users', App\Http\Controllers\Api\v1\UserController::class)
->only(['index','show','destroy','store','update']);

Route::apiResource('v1/suma', App\Http\Controllers\Api\v1\SumaController::class)
->only(['store','index']);