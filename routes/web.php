<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [App\Http\Controllers\ProductosController::class, 'index']);
Route::get('/productos_update/{id}', [App\Http\Controllers\ProductosController::class, 'update']);
Route::get('/productos_delete/{id}', [App\Http\Controllers\ProductosController::class, 'destroy']);
Route::get('/productos_new', [App\Http\Controllers\ProductosController::class, 'create']);
Route::post('/save_product', [App\Http\Controllers\ProductosController::class, 'store']);
Route::post('/product_update/{id}', [App\Http\Controllers\ProductosController::class, 'edit']);




Route::get('/ejemplo', function (Request $request) {
    return response()->json(['mensaje' => '¡Hola desde la API!']);
});